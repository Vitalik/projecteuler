<?php
/**
 * Created by PhpStorm.
 * User: Vitalie.Morozan
 * Date: 12/9/2015
 * Time: 4:15 PM
 */

namespace Solver\Listener;

use Solver\Helper\General;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class CommandListener
 */
class CommandListener
{
    /**
     * @var Stopwatch
     */
    protected static $stopwatch;

    /**
     * @param ConsoleCommandEvent $event
     */
    public function onCommand(ConsoleCommandEvent $event)
    {
        static::$stopwatch = new Stopwatch();
        static::$stopwatch->start('command');
    }

    /**
     * @param ConsoleTerminateEvent $event
     */
    public function onTerminate(ConsoleTerminateEvent $event)
    {
        $stats = static::$stopwatch->stop('command');
        $output = $event->getOutput();

        $output->writeln(sprintf('Time: %s', $stats->getDuration() / 1000));
        $output->writeln(sprintf('Memory: %s', General::humanizeMemorySize($stats->getMemory())));
    }
}
