<?php
/**
 * Created by PhpStorm.
 * User: Vitalie.Morozan
 * Date: 12/9/2015
 * Time: 4:33 PM
 */

namespace Solver\Command;

use Solver\Helper\General;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Problem052
 */
class Problem052 extends Main
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $n = 10;

        while(!$this->isPermutedMultiple($n++, 6)) {}

        $output->writeln(--$n);

        return 0;
    }

    protected function isPermutedMultiple($number, $permutations): bool
    {
        $length = strlen($number);
        $digits = General::countDigits($number);

        for ($i = 2; $i <= $permutations; $i++) {
            $next = $number * $i;
            $nextLength = strlen($next);
            $nextDigits = General::countDigits($next);

            if ($nextLength > $length
                || array_diff_assoc($digits, $nextDigits)
                || array_diff_assoc($nextDigits, $digits)
            ) {
                return false;
            }
        }

        return true;
    }
}
