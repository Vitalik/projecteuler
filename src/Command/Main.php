<?php
/**
 * Created by PhpStorm.
 * User: Vitalie.Morozan
 * Date: 12/9/2015
 * Time: 1:43 PM
 */

namespace Solver\Command;

use Symfony\Component\Console\Command\Command;

/**
 * Class Main
 */
class Main extends Command
{
    protected function configure()
    {
        preg_match('/(?<name>problem(?<id>\d+))/i', get_class($this), $matches);

        if (isset($matches['name'], $matches['id'])) {
            $this
                ->setName(strtolower($matches['name']))
                ->setDescription(sprintf('Problem %d', $matches['id']));
        }
    }
}
