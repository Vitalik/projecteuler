<?php
/**
 * Created by PhpStorm.
 * User: Vitalie.Morozan
 * Date: 12/9/2015
 * Time: 4:33 PM
 */

namespace Solver\Command;

use Solver\Helper\Math;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Problem039
 */
class Problem039 extends Main
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $perimeter = 0;
        $maxSolutions = 0;

        for ($i = 1; $i <= 1000; $i++) {
            $countSolutions = count($this->findRightAngleTrianglesByPerimeter($i));

            if ($countSolutions > $maxSolutions) {
                $maxSolutions = $countSolutions;
                $perimeter = $i;
            }
        }

        $output->writeln($perimeter);

        return 0;
    }

    protected function findRightAngleTrianglesByPerimeter(int $perimeter): array
    {
        if ($perimeter < 4) {
            return [];
        }

        $solutions = [];
        $limit = floor($perimeter / (2 + sqrt(2)));

        for ($i = 1; $i <= $limit; $i++) {
            $j = ($perimeter ** 2 / 2 - $perimeter * $i) / ($perimeter - $i);

            if ((int) $j == $j && Math::isWholeNumber($k = sqrt($i ** 2 + $j ** 2))) {
                $solutions[] = [$i, $j, $k];
            }
        }

        return $solutions;
    }
}
