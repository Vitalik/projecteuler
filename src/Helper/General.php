<?php
/**
 * Created by PhpStorm.
 * User: Vitalie.Morozan
 * Date: 12/9/2015
 * Time: 4:48 PM
 */

namespace Solver\Helper;

/**
 * Class General
 */
class General
{
    public static function countDigits(int $n): array
    {
        return array_count_values(str_split($n));
    }

    public static function humanizeMemorySize($n): string
    {
        $units = explode(',', ',K,M,G,T,P,E,Z,Y');
        $length = end($units);
        reset($units);

        while ($n >= 1024 && current($units) != $length) {
            $n /= 1024;
            next($units);
        }

        return round($n, 1) . current($units) . 'B';
    }
}
