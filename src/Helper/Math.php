<?php
/**
 * Created by PhpStorm.
 * User: Vitalie.Morozan
 * Date: 12/9/2015
 * Time: 4:31 PM
 */

namespace Solver\Helper;

/**
 * Class Math
 */
class Math
{
    public static function isWholeNumber($n): bool
    {
        return (int) $n == $n;
    }
}
