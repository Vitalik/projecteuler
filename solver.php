<?php

require __DIR__.'/vendor/autoload.php';

use Solver\Listener\CommandListener;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\EventDispatcher\EventDispatcher;

$listener = new CommandListener();

$dispatcher = new EventDispatcher();
$dispatcher->addListener(ConsoleEvents::COMMAND, [$listener, 'onCommand']);
$dispatcher->addListener(ConsoleEvents::TERMINATE, [$listener, 'onTerminate']);

$application = new Application();
$application->setDispatcher($dispatcher);

foreach (glob('src/Command/Problem*.php') as $path) {
    preg_match('/(?<c>problem\d+)/i', $path, $matches);

    if (isset($matches['c']) && class_exists($class = sprintf('Solver\Command\%s', $matches['c']))) {
        $application->add(new $class);
    }
}

$application->run();
